package org.bukkit.command;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertEquals;

import org.junit.*;

import java.util.Set;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.Plugin;
import org.bukkit.Server;
import org.bukkit.Bukkit;
import org.bukkit.TestServer;
import org.bukkit.command.CommandSender;

interface MockCommandSender extends CommandSender {
    public String getLastMessage();
}
public class FormattedCommandAliasTest {
    private String INVALID_REPLACEMENT_TOKEN = "Invalid replacement token";
    private String INTERNAL_ERROR =
        org.bukkit.ChatColor.RED + "An internal error occurred while attempting to perform this command";
    private String MISSING_REQUIRED_ARGUMENT = "Missing required argument ";

    private static final Server server = TestServer.getInstance();

    MockCommandSender mockCommandSender = new MockCommandSender() {
            private String lastMessage;

            public String getLastMessage() { return lastMessage; }
            public void sendMessage(String message) { lastMessage = message; }
            public void sendMessage(String[] messages) {}
            public Server getServer() { return null; }
            public String getName() { return "mock command sender"; }

            public boolean isPermissionSet(String name) {
                return false;
            }

            public boolean isPermissionSet(Permission perm) {
                return false;
            }

            public boolean hasPermission(String name) {
                return false;
            }

            public boolean hasPermission(Permission perm) {
                return false;
            }

            public PermissionAttachment addAttachment(Plugin plugin, String name, boolean value) {
                return null;
            }

            public PermissionAttachment addAttachment(Plugin plugin) {
                return null;
            }

            public PermissionAttachment addAttachment(Plugin plugin, String name, boolean value, int ticks) {
                return null;
            }

            public PermissionAttachment addAttachment(Plugin plugin, int ticks) {
                return null;
            }

            public void removeAttachment(PermissionAttachment attachment) {

            }

            public void recalculatePermissions() {

            }

            public Set<PermissionAttachmentInfo> getEffectivePermissions() {
                return null;
            }

            public boolean isOp() {
                return false;
            }

            public void setOp(boolean value) { }
        };

    @Test
    public void testEmpty() {
        FormattedCommandAlias fca = new FormattedCommandAlias("dummy", new String[0]);
        mockCommandSender.sendMessage("zzz");
        fca.execute(mockCommandSender, "", new String[0]);
        assertEquals("zzz", mockCommandSender.getLastMessage());
    }

    // add more tests here to achieve 100% statement coverage
}
