// this example is from https://www.toolsqa.com/rest-assured/rest-api-test-using-rest-assured/

// sample code also at https://semaphoreci.com/community/tutorials/testing-rest-endpoints-using-rest-assured
// but a better resource is https://github.com/rest-assured/rest-assured/wiki/GettingStarted
// also https://www.baeldung.com/rest-assured-tutorial

package se465;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import org.junit.*;

public class SampleTest {
    
    @Test
    public void GetWeatherDetails()
    {   
        RestAssured.baseURI = "https://jsonplaceholder.typicode.com";
 
        RequestSpecification httpRequest = RestAssured.given();
        Response response = httpRequest.request(Method.GET, "/todos/1");
 
        String responseBody = response.getBody().asString();
        System.out.println("Response Body is =>  " + responseBody);
    }
}
