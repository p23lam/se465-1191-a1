#!/usr/bin/env bash
case $(id -u) in
    0)
        # install java 8
        # automatically accept license
        echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
        apt-add-repository ppa:webupd8team/java
        apt-get update
        apt-get -y upgrade
        apt-get install -y oracle-java8-installer
        apt-get install -y oracle-java8-set-default
        apt-get install -y junit4 maven git

        ;;
    *)
        ;;
esac
